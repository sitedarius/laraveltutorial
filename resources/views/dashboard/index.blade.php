@extends ('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <h1>Dashboard</h1>
            </div>
            <div class="col-md-8 row">
                <div class="col-md-6 mb-4">
                    <a href="/projecten/" class="project-card">
                        <div class="card border-custom">
                            <div class="card-body">
                                <h3>Mijn eerste project</h3>
                                <hr>
                                <p>Dit is mijn allereerste project</p>
                            </div>
                            <div class="card-footer text-center">
                                <p class="mb-0">Aangemaakt
                                    op: 29-12-1999</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 mb-4">
                    <a href="/projecten/" class="project-card">
                        <div class="card border-custom">
                            <div class="card-body">
                                <h3>Mijn tweede project</h3>
                                <hr>
                                <p>Dit is mijn tweede project</p>
                            </div>
                            <div class="card-footer text-center">
                                <p class="mb-0">Aangemaakt
                                    op: 29-12-1999</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card border-custom">
                    <div class="card-body row">
                        <div class="col-md-4 col-4">
                            <img src="/storage/" alt="" class="w-100">
                        </div>
                        <div class="col-md-8 col-8">
                            <p class="m-0">Rick Visser</p>
                            <hr class="my-2">
                            <span class="badge badge-pill badge-info">Functie</span>
                            <span class="badge badge-pill badge-info">Functie</span>
                            <span class="badge badge-pill badge-info">Functie</span>
                            <hr class="my-2">
                            <p class="m-0">Email</p>
                            <p class="m-0">Phones</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
