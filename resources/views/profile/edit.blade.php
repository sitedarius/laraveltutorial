@extends ('layouts.app')

@section ('title', 'Profiel bewerken - Spoofy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Profiel bewerken</h1>
            </div>
            <div class="col-md-8 mt-4 mb-4">
                <div class="card border-custom">
                    <div class="card-body">
                        <form method="GET" action="/profiel/{user}/update">
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                                </div>
                                <div class="form-group col-6">
                                    <label for="username">Gebruikersnaam:</label>
                                    <input type="text" class="form-control" id="username" nme="username" value="{{ $user->username }}">
                                </div>
                                <div class="form-group col-6">
                                    <label for="firstname">Voornaam:</label>
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}">
                                </div>
                                <div class="form-group col-6">
                                    <label for="lastname">Achternaam:</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname }}">
                                </div>
                                <div class="form-group col-12">
                                    <label for="street">Straat:</label>
                                    <input type="text" class="form-control" id="street" name="street" value="{{ $user->street }}">
                                </div>
                                <div class="form-group col-4">
                                    <label for="number">Huisnummer:</label>
                                    <input type="text" class="form-control" id="number" name="number" value="{{ $user->number }}">
                                </div>
                                <div class="form-group col-4">
                                    <label for="addon">Toevoeging:</label>
                                    <input type="text" class="form-control" id="addon" name="addon" value="{{ $user->addon }}">
                                </div>
                                <div class="form-group col-4">
                                    <label for="zipcode">Postcode:</label>
                                    <input type="text" class="form-control" id="zipcode" name="zipcode" value="{{ $user->zipcode }}">
                                </div>
                                <div class="form-group col-4">
                                    <label for="phone">Telefoon:</label>
                                    <input type="tel" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                                </div>
                                <div class="form-group col-4">
                                    <label for="birthday">Verjaardag:</label>
                                    <input type="date" class="form-control" id="birthday" name="birthday" value="{{ $user->birthday }}">
                                </div>
                                <div class="form-group col-4">
                                    <label for="birthday">Kleur:</label>
                                    <input type="color" class="form-control" id="birthday" name="birthday" value="{{ $user->birthday }}">
                                </div>
                                <div class="form-group col-12">
                                    <label for="job">Functie:</label>
                                    <input type="text" class="form-control" id="job" name="job" value="{{ $user->job }}">
                                </div>
                                <div class="form-group col-12">
                                    <label for="password">Wachtwoord:</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                                <div class="form-group col-12">
                                    <label for="repeat">Herhaal Wachtwoord:</label>
                                    <input type="password" class="form-control" id="repeat" name="repeat_password">
                                </div>
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-primary">Opslaan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-4">
                <div class="card border-custom">
{{--                    <img src="" alt="" class="w-100">--}}
                    <div class="card-body row">
                        <div class="col-md-12">
                            <form>
                                <div class="form-group">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <button type="button" class="btn btn-danger">Annuleren</button>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <form>
                                <button type="submit" class="btn btn-success">Uploaden</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection