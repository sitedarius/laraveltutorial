@extends ('layouts.app')

@section ('title', '{project->title}')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <h1>Profiel</h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('profiel/bewerken') }}" class="btn btn-success">Bewerken</a>
            </div>
        </div>
    </div>

@endsection