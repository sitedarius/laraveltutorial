@extends ('layouts.app')

@section ('title', 'Mijn Projecten - Spoofy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Projecten</h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="/projecten/aanmaken" class="btn btn-success">Project aanmaken</a>
            </div>
            @foreach ($projects as $project)
                <div class="col-md-4 mt-3">
                    <a href="/projecten/{{ $project->id }}" class="project-card">
                        <div class="card border-custom">
                            <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}" alt="{{ $project->cover_image }}" class="w-100">
                            <div class="card-body">
                                <h3>{{ $project->title }}</h3>
                                <hr>
                                <p>{{ $project->user->name }}</p>
                                <p>{{ $project->description_short }}</p>
                            </div>
                            <div class="card-footer text-center">
                                <p class="mb-0">Aangemaakt
                                    op: {{ Carbon\Carbon::parse($project->created_at)->format('d-m-Y') }}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

        </div>
    </div>

@endsection