@extends ('layouts.app')

@section ('title', '{project->title}')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 mb-4">
                <h1>{{ $project->title }}</h1>
            </div>
            <div class="col-md-2 text-right mb-4">
                <a href="/projecten/{{ $project->id }}/bewerken" class="btn btn-success">Bewerken</a>
            </div>
            <div class="col-md-8 mb-4">
                <div class="card border-custom ">
                    {{--                    <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}" alt="{{ $project->cover_image }}" class="w-100">--}}
                    <div class="card-body">
                        {!! $project->description_long !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card border-custom">
                    <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}"
                         alt="{{ $project->cover_image }}" class="w-100">
                    <div class="card-body">
                        <h6 class="m-0">{{ $project->user->firstname }} {{ $project->user->lastname }}</h6>
{{--                        @foreach ($project->jobs as $job)--}}
                            <span class="badge badge-pill badge-info m-0">{{ $project->user->job }}</span>
{{--                        @endforeach--}}
                        <hr class="mx-2">
                        <li>{{ $project->user->username }}</li>
                        <li>{{ $project->user->phone }}</li>
                        <li>{{ $project->user->email }}</li>
                        @if ($project->tasks->count())
                            <hr>
                            <h6>Taken:</h6>
                            @foreach ($project->tasks as $task)
                                <form method="POST" action="/tasks/{{ $task->id }}">
                                    @method('PATCH')
                                    @csrf
                                    <label class="checkbox {{ $task->completed ? 'is-complete' : '' }}"
                                           for="completed">
                                        <input type="checkbox" name="completed"
                                               onChange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}>
                                        {{ $task->title }}
                                    </label>
                                </form>
                            @endforeach
                        @endif
                        <hr>
                        <form method="POST" action="/projecten/{{ $project->id }}/tasks">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Taak toevoegen:</label>
                                <input type="text" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp" name="title" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Opslaan</button>
                        </form>

                        @include ('errors')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection