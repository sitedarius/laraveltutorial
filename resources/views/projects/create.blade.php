@extends ('layouts.app')

@section ('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h1>Maak een project aan</h1>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <p>Vul alle velden in:</p>

                        <form method="POST" action="/projecten">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Titel:*</label>
                                <input type="text"
                                       class="form-control {{ $errors->has('title') ? 'is_invalid' : 'is_valid' }}"
                                       name="title" id="exampleFormControlInput1" placeholder="Project 1..." required
                                       value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Korte Beschrijving:*</label>
                                <input type="text"
                                       class="form-control {{ $errors->has('description_short') ? 'is_invalid' : 'is_valid' }}"
                                       name="description_short" id="exampleFormControlInput1"
                                       placeholder="Dit is mijn project..." required
                                       value="{{ old('description_short') }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Tekst:*</label>
                                <textarea
                                        class="description {{ $errors->has('description_long') ? 'is_invalid' : 'is_valid' }}"
                                        name="description_long"></textarea>
                                <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                <script>
                                    tinymce.init({
                                        selector: 'textarea.description',
                                        height: 500
                                    });
                                </script>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <small>* Verplichte velden</small>
                                    <button type="submit" class="btn btn-primary">Opslaan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body row">
                        <div class="col-md-12">
                            <form>
                                <div class="form-group">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <button type="button" class="btn btn-danger">Annuleren</button>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <form>
                                <button type="submit" class="btn btn-success">Uploaden</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection