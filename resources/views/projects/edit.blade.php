@extends ('layouts.app')

@section ('title', 'Bewerken')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Project bewerken</h1>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="/projecten/{{ $project->id }}/update">
                            @method('PATCH')
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Titel:*</label>
                                <input type="text" class="form-control" name="title" id="exampleFormControlInput1"
                                       placeholder="Project 1..." value="{{ $project->title }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Korte Beschrijving:*</label>
                                <input type="text" class="form-control" name="description_short"
                                       id="exampleFormControlInput1" placeholder="Dit is mijn project..."
                                       value="{{ $project->description_short }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Tekst:*</label>
                                <textarea class="description"
                                          name="description_long">{{ $project->description_long ? $project->description_long : old('description_long') }}</textarea>
                                <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                <script>
                                    tinymce.init({
                                        selector: 'textarea.description',
                                        height: 500
                                    });
                                </script>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <small>* Verplichte velden</small>

                                    <button type="submit" class="btn btn-primary">Opslaan</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}" alt="{{ $project->cover_image }}" class="w-100">
                    <div class="card-body row">
                        <div class="col-md-12">
                            <form>
                                <div class="form-group">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <button type="button" class="btn btn-danger">Annuleren</button>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <form>
                                <button type="submit" class="btn btn-success">Uploaden</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection