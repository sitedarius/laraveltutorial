<!DOCTYPE html>
<html lang="NL">

<head>
    <title>@yield ('title', 'Welkom - Spoofy')</title>
    <link rel="icon" href="/public/favicon.ico" type="image/x-icon">

    {{--Custom css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.new.css') }}">

    {{--Bootstrap--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    {{--FontAwesome--}}
    <script src="https://kit.fontawesome.com/6f85a7f239.js" crossorigin="anonymous"></script>

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark border-green">
    <div class="container">
        <a class="navbar-brand" href="/">Laravel Tutorial</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form action="" class="ml-auto mr-auto">
                <div class="p-1 bg-light rounded rounded-pill shadow-sm">
                    <div class="input-group">
                        <input type="search" placeholder="Zoeken..." aria-describedby="button-addon1" class="form-control rounded-search border-0 bg-light">
                        <div class="input-group-append">
                            <button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="{{ url('profiel') }}" class="nav-link"><i class="fas fa-user"></i></a>
                </li>
                <li class="nav-item">
                    <a href="/help" class="nav-link"><i class="far fa-question-circle"></i></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/logout') }}" class="nav-link"><i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<nav class="navbar navbar-expand navbar-light bg-light">
    <div class="container">
        <ul class="navbar-nav ml-auto mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/"><i class="fas fa-columns"></i> - Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/projecten"><i class="fas fa-tasks"></i> - Projecten</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bestanden"><i class="fas fa-folder"></i> - Bestanden</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/agenda"><i class="far fa-calendar-alt"></i> - Agenda</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bedrijven"><i class="fas fa-store"></i> - Bedrijven</a>
            </li>
        </ul>
    </div>
</nav>

@include ('errors')

<div class="mt-5">
    @yield ('content')
</div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</html>
