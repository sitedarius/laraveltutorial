<!DOCTYPE html>
<html lang="NL">

<head>
    <title>@yield ('title', 'Welkom niffo')</title>

    {{--Custom css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.new.css') }}">

    {{--Bootstrap--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    {{--FontAwesome--}}
    <script src="https://kit.fontawesome.com/6f85a7f239.js" crossorigin="anonymous"></script>

</head>

<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1">
            <div class="navbar-left">
                <div class=" mt-auto mb-auto">
                    <ul class="navbar-nav mt-4">
                        <li class="nav-item">
                            <a class="nav-link" href="/"><i class="fas fa-home"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/over-ons"><i class="fas fa-address-card"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/projecten"><i class="fas fa-tasks"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/contact"><i class="fas fa-phone-alt"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/"><i class="fas fa-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/"><i class="far fa-question-circle"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/"><i class="fas fa-sign-out-alt"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-11">
            <div class="mt-5 mb-5">
                @yield ('content')
            </div>
        </div>
    </div>
</div>


</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</html>
