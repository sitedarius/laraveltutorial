<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->middleware('auth');;

Route::get('/projecten', 'ProjectsController@index')->middleware('auth');;
Route::get('/projecten/aanmaken', 'ProjectsController@create')->middleware('auth');;
Route::get('/projecten/{project}', 'ProjectsController@show')->middleware('auth');;
Route::post('/projecten', 'ProjectsController@store')->middleware('auth');;
Route::get('/projecten/{project}/bewerken', 'ProjectsController@edit')->middleware('auth');;
Route::patch('/projecten/{project}/update', 'ProjectsController@update')->middleware('auth');;
Route::delete('/projecten/{project}', 'ProjectsController@destroy')->middleware('auth');;

Route::post('/projecten/{project}/tasks', 'ProjectsTasksController@store')->middleware('auth');;
Route::patch('/tasks/{task}', 'ProjectsTasksController@update')->middleware('auth');;

Route::get('/help', 'PagesController@help')->middleware('auth');;

Route::get('/bedrijven', 'PagesController@company')->middleware('auth');;

Route::get('/bestanden', 'PagesController@files')->middleware('auth');;

Route::get('/agenda', 'PagesController@agenda')->middleware('auth');;

Auth::routes();

Route::get('/logout', 'AuthController@logout');

Route::get('/profiel', 'ProfileController@show')->middleware('auth');;
Route::get('/profiel/bewerken', 'ProfileController@edit')->middleware('auth');;
Route::patch('/profiel/bewerken', 'ProfileController@update')->middleware('auth');;