<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function show()
    {
        return view('profile.show');
    }

    public function edit(User $user)
    {
        return view('profile.edit', compact('user'));
    }

    public function update(User $user)
    {
        $user->update(request(['firstname, lastname, username, email, job']));

        return redirect('/profiel');
    }
}
