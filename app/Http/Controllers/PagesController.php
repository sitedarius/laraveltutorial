<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $projects = \App\Project::all();

        return view('dashboard.index', compact('projects'));
    }

    public function help()
    {
        return view('help.index');
    }

    public function company()
    {
        return view('company.index');
    }

    public function files()
    {
        return view('files.index');
    }

    public function agenda()
    {
        return view('agenda.index');
    }
}
