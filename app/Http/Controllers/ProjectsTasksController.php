<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Project;

class ProjectsTasksController extends Controller
{
    public function store(Project $project)
    {
        request()->validate(['title' => 'required']);

        Task::create([
            'project_id' => $project->id,
            'title' => request('title')
        ]);

        return back();
    }

    public function update(Task $task)
    {
        $task->update([
            'completed' => request()->has('completed')
        ]);

        return back();
    }
}
