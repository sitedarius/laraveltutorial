<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    public function index()
    {
        $projects = \App\Project::all();

        return view('projects.index', compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function show(Project $project)
    {
        return view('projects.show', compact('project'));
    }

    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    public function update(Project $project)
    {
        $project->update(request(['title, description_short, description_long, cover_image']));

        return redirect('/projecten');
    }

    public function destroy(Project $project)
    {
        $project->delete();

        return redirect('/projecten');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'min:10'],
            'description_short' => ['required', 'min:10', 'max:30'],
            'description_long' => ['required', 'min:10']
        ]);

        $project = new Project();
        $project->title = $data['title'];
        $project->description_short = $data['description_short'];
        $project->description_long = $data['description_long'];
        $project->user_id = Auth::user()->id;
        $project->save();

//        Project::create(request(['title', 'description_short', 'description_long', 'cover_image']));

        return redirect('/projecten');
    }
}
